﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Assets.Scripts.ITEMS;

public class StatManager : MonoBehaviour
{
    public Dictionary<string, int> StatDict = new Dictionary<string, int>();
    public GameObject player;

    void Awake()
    {
        //Testzwecke
        StatDict.Add("Health", 1);
        StatDict.Add("Speed", 1);
        StatDict.Add("Size", 1);
        StatDict.Add("KritChance", 1);
        StatDict.Add("KritSchaden", 1);
        StatDict.Add("Luck", 1);
    }

    public void LoadStatsFromLocalStorage()
    {
        //Get From Storage and Set on start
        StatDict["Health"] = 1;
        StatDict["Speed"] = 1;
        StatDict["Size"] = 2;
        StatDict["KritChance"] = 1;
        StatDict["KritSchaden"] = 1;
        StatDict["Luck"] = 1;
    }

    void SetStatsToDefault()
    {

    }



    public void UpdateStats(List<Stat> statList)
    {
        //sortiere Enums nach größe
        for (int i = 0; i < statList.Count; i++) {
            switch (statList[i].GetStatName())
            {
                case EnumStat.Health: StatDict["Health"] += (int)statList[i].GetValue(); break;
                case EnumStat.Speed: StatDict["Speed"] += (int)statList[i].GetValue(); break;
                case EnumStat.Size: StatDict["Size"] += (int)statList[i].GetValue(); break;
                case EnumStat.KritChance: StatDict["KritChance"] += (int)statList[i].GetValue(); break;
                case EnumStat.KritSchaden: StatDict["KritSchaden"] += (int)statList[i].GetValue(); break;
                case EnumStat.Luck: StatDict["Luck"] += (int)statList[i].GetValue(); break;
            }
          
        }
        player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerStats>().UpdateStats(StatDict);
    }
}
