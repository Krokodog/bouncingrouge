﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemProperties : MonoBehaviour {

    // General item properties are defined here, like falling and collision with player.

	// Use this for initialization
	void Start () {
		
	}


    // Update is called once per frame
    void Update()
    {
        transform.position = this.transform.position + Vector3.down * Time.deltaTime * GameManager.Instance.itemSpeed;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBarMovement>().ItemEvaluation(this.gameObject.tag);
            Destroy(this.gameObject);
        }

        if (col.tag == "LevelDead")
        {
            Destroy(this.gameObject);
        }

    }
}
