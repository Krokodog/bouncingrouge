﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {


    public AudioSource blockDestroySource;
    public AudioSource collisionBallSource;
    public AudioSource coinSource;
    public AudioSource musicSource;
    public AudioSource itemBombSource;

    public static SoundManager instance = null;



    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {       
        Destroy(gameObject);
        }

   
        DontDestroyOnLoad(gameObject);
    }


    //Used to play single sound clips.
    public void PlaySingle(AudioSource selectedSource)
    {
        
        selectedSource.Play();
    }


 
}
