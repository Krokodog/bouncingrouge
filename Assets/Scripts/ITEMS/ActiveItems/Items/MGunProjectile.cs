﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MGunProjectile: MonoBehaviour {

    private float mgunSpeed = 3f;


	// Use this for initialization
	void Start () {
        
	}

    // Update is called once per frame
    void Update()
    {
        transform.position = this.transform.position + Vector3.up * Time.deltaTime * mgunSpeed;
    }

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.tag == "Enemy")
        {
            col.gameObject.GetComponent<BlockManager>().TakeDamage(GameManager.Instance.mGunDamage);
            Destroy(this.gameObject);
        }

        if (col.tag == "Level")
        {
            Destroy(this.gameObject);
        }

    }

}
