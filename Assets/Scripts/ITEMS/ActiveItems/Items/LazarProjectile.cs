﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.ITEMS;
using UnityEngine;

public class LazarProjectile : MonoBehaviour
{
    public LineRenderer lr;
    private Transform lazerSpawn;
    private Vector3 spawnPosition;


    // Use this for initialization
    void Start()
    {
        Debug.Log("spawn projectile laser");
        spawnPosition = GameObject.FindObjectOfType<ItemActiveGeneral>().transform.position;
        this.gameObject.transform.position = spawnPosition;
        Debug.Log(spawnPosition);
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.position += new Vector3(0, 10, 0) * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("boom");
        if (col.gameObject.tag == "Level")
        {
            if (!GameManager.Instance.gameOver)
            {
               Destroy(this.gameObject);

            }
        }
        if (col.gameObject.tag == "Enemy")
        {
            col.gameObject.GetComponent<BlockManager>().TakeDamage(GameManager.Instance.playerDamage);
        }

    }
}
