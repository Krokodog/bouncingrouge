﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.CONTROLLER;
using Assets.Scripts.ITEMS;
using UnityEngine;

public class MGunItem : ItemActiveGeneral
{

    public new string Name { get; set; }
    public EnumItemTyp ItemTyp { get; set; }
    public override int Cooldown { get; set; }


    public MGunItem()
    {
        this.Name = "MGunItem";
        this.ItemTyp = EnumItemTyp.Active;
        this.Cooldown = 5;
    }

    void Start ()
    {
        this.gameObject.transform.localPosition = Vector3.zero;
        Debug.Log(this.Name+","+this.ItemTyp+","+this.Cooldown);
    }

    public override void TriggerAbility()
    {
        Debug.Log("Shoot MGun");
    }

}
