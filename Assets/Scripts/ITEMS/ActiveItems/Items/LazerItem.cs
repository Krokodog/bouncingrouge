﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.CONTROLLER;
using Assets.Scripts.ITEMS;
using Unity.UNetWeaver;
using UnityEngine;

public class LazerItem : ItemActiveGeneral
{
    public new string Name { get; set; }
    public new EnumItemTyp EnumItemTyp { get; set; }
    public override int Cooldown { get; set; }

    public GameObject projectile;
    

    public LazerItem() { 

        this.Name = "Laser";
        this.EnumItemTyp = EnumItemTyp.Active;
        this.Cooldown = 5;
    }

    // Use this for initialization
    void Start ()
    {
        this.gameObject.transform.localPosition = Vector3.zero;
        Debug.Log(this.Name+","+this.EnumItemTyp+","+this.Cooldown);
    }

    public override void TriggerAbility()
    {
        Debug.Log("Shoot Laser");
        if (projectile != null)
        {
            projectile = Instantiate(projectile);
        }
    }

}
