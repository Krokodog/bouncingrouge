﻿namespace Assets.Scripts.ITEMS
{
    public interface IItemGeneral
    {
        string Name { get; set; }

        EnumItemTyp EnumItemTyp { get; set; }
    }
}