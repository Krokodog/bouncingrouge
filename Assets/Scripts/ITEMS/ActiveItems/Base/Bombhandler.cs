﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bombhandler : MonoBehaviour
{

    public GameObject explosionEffect;

    void BombExploded()
    {
        SoundManager.instance.PlaySingle(SoundManager.instance.itemBombSource);
        Instantiate(explosionEffect, this.transform.position, this.transform.rotation);
        Collider2D[] box = Physics2D.OverlapCircleAll(this.transform.position, 1);

        foreach (var enemy in box)
        {
            if(enemy.tag == "Enemy")
            {
                enemy.gameObject.GetComponent<BlockManager>().TakeDamage(GameManager.Instance.bombDamage);
            }

        }
        Destroy(this.gameObject, 1f);

    }


}
