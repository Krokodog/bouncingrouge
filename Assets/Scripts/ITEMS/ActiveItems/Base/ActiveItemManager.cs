﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.CONTROLLER;
using Assets.Scripts.ITEMS;
using UnityEngine;
using UnityEngine.UI;

public class ActiveItemManager : MonoBehaviour {

    // Sprite sizes
    public Sprite[] activeItems;
    public SpriteRenderer activeItemSprite;
    public Image coolDownImage;
    private bool startingCD;
    private float timer;
    private int counter;


    private GameObject instanceItem;
    private int cooldownForActiveItem;
    private ItemGeneral activeItemGameObject;
    private readonly IActiveItemController activeItemController;
    private string itemName;
    private bool isCoolDownOver = true;
    [SerializeField]
    private int currCoolDownOfActiveItem;
    private Coroutine CooldownCoroutine;

    void Start()
    {
        Debug.Log("ActiveItemManager attached");
    }

    public void setActiveItem(string itemName)
    {
        this.itemName = itemName;
        if (itemName == "Lazer")
        {
            DestroyCurrItem();
            instanceItem = Instantiate(Resources.Load("Prefabs/LaserWeapon", typeof(GameObject))) as GameObject;
        }

        if (itemName == "M_Gun")
        {
            DestroyCurrItem();
            instanceItem = Instantiate(Resources.Load("Prefabs/CannonsWeapon", typeof(GameObject))) as GameObject;
        }
    }

    public void callTriggerActiveItem()
    {
        if (instanceItem != null)
        {
            instanceItem.GetComponent<ItemActiveGeneral>().TriggerAbility();
        }
    }

    void Update()
    {

        if (instanceItem != null)
        {
            instanceItem.transform.localPosition = this.gameObject.transform.position;
            if (isCoolDownOver)
            {
                isCoolDownOver = false;
                StartCooldownActiveTime();
            }
        }

        if (startingCD && instanceItem != null)
        {
            Debug.Log("call me once");
            currCoolDownOfActiveItem = instanceItem.GetComponent<ItemActiveGeneral>().Cooldown;
            startingCD = false;
            CooldownCoroutine = StartCoroutine(ExecuteCoolDown());
        }

    }

    public void SetCurrentActiveItem(int selectActiveItem)
    {
        activeItemSprite.sprite = activeItems[selectActiveItem];
    }

    public void StartCooldownActiveTime()
    {
        counter = 0;
        startingCD = true;
    }

    public void DestroyCurrItem()
    {
        if (instanceItem != null)
        {
            StopCoroutine(CooldownCoroutine);
            Destroy(instanceItem);
            isCoolDownOver = true;
            //coolDownImage.fillAmount = 1;
        }
    }

    private IEnumerator ExecuteCoolDown()
    {
        counter++;
        yield return new WaitForSeconds(currCoolDownOfActiveItem);
        //coolDownImage.fillAmount = (timer - counter*refreshRate)/timer;
        isCoolDownOver = true;
        Destroy(instanceItem);
    }
}
