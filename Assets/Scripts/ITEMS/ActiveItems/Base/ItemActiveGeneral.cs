﻿using UnityEngine;

namespace Assets.Scripts.ITEMS
{
    public abstract class ItemActiveGeneral : ItemGeneral
    {
       public abstract void TriggerAbility();

       public abstract int Cooldown { get; set; }
    }
}