﻿using UnityEngine;

namespace Assets.Scripts.ITEMS
{
    public class ItemGeneral : MonoBehaviour, IItemGeneral
    {
        public string Name { get; set; }
        public EnumItemTyp EnumItemTyp { get; set; }
    }
}