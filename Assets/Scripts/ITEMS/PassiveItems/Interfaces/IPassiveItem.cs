﻿using UnityEngine;
namespace Assets.Scripts.ITEMS
{
    public interface IPassiveItem
    {
        void TriggerAbilityOfPassiveItem();
    }
}
