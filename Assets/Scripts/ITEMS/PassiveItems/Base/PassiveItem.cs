﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.ITEMS;

[CreateAssetMenu(fileName = "New PassivItem", menuName ="PassiveItem")]
public class PassiveItem : ScriptableObject
{
    public new string name;
    public string discription;
    public List<Stat> stats;
    public Sprite image;
}
