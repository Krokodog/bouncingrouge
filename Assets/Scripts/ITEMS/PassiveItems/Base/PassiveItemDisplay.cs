﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.ITEMS;

public class PassiveItemDisplay : MonoBehaviour
{

    public PassiveItem passiveItem;

    public new string name;
    public string disciption;
    public List<Stat> stats;
    public Sprite image;


    void Start()
    {
        this.name = passiveItem.name;
        this.disciption = passiveItem.discription;
        this.stats = passiveItem.stats;
        this.image = this.GetComponent<SpriteRenderer>().sprite = passiveItem.image;
      
    }

    void Update()
    {
        transform.position = this.transform.position + Vector3.down * Time.deltaTime * GameManager.Instance.itemSpeed;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!GameManager.Instance.gameOver)
            {
                TriggerAbilityOfPassiveItem(stats);
                Destroy(this.gameObject);
            }
        }
        if (col.gameObject.tag == "LevelDead")
        {
            Destroy(this.gameObject);
        }

    }

    private void TriggerAbilityOfPassiveItem(List<Stat> stats)
    {
        GameManager.Instance.statManager.UpdateStats(stats);
    }

}
