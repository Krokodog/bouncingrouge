﻿using UnityEngine;

[System.Serializable]
public class Stat
{
    [SerializeField]
    private EnumStat statName;
    [SerializeField]
    private float baseValue;


    public EnumStat GetStatName()
    {
        return statName;
    }

    public float GetValue()
    {
        return baseValue;
    }
}
