﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public float health = 0;
    public float speed = 0;
    public float size = 0;
    public float kritChance = 0;
    public float kritSchaden = 0;
    public float luck = 0;

    public Sprite[] differentSizes;

    StatManager statManager = new StatManager();
    Dictionary<string, int> playerStat;

    void Start()
    {
        statManager.LoadStatsFromLocalStorage();
        playerStat = statManager.StatDict;
        GetStats();

        GetComponent<SpriteRenderer>().sprite = differentSizes[(int)this.size];
    }

    public void ChangePlayerSize()
    {
        GetComponent<SpriteRenderer>().sprite = differentSizes[(int)this.size];
        Destroy(GetComponent<BoxCollider2D>());
        gameObject.AddComponent<BoxCollider2D>();
    }

    public void UpdateStats(Dictionary<string,int> dictStats)
    {
        playerStat = dictStats;
        GetStats();
        ChangePlayerSize();
    }

    public void GetStats()
    {
        this.health = playerStat["Health"];
        this.speed = playerStat["Speed"];
        this.size = playerStat["Size"];
        this.kritChance = playerStat["KritChance"];
        this.kritSchaden = playerStat["KritSchaden"];
        this.luck = playerStat["Luck"];
    }

}
