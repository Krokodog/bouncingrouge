﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopHandler : MonoBehaviour
{
    public Slider curSlider;

    // Start is called before the first frame update
 
    public void UpgradeStat()
    {
        curSlider.value += 1;
    }
    public void DowngradeStat()
    {
        curSlider.value -= 1;
    }
    public void ReturnToMenu()
    {
        SceneManager.LoadScene(1);

    }
}
