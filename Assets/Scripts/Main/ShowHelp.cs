﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class ShowHelp : MonoBehaviour
{
    public GameObject ScrollView;
    private bool showScrollView;

    // Start is called before the first frame update
    void Start()
    {
        ScrollView.SetActive(showScrollView);
    }
    
    public void OpenHelp()
    {
        showScrollView = !showScrollView;
        ScrollView.SetActive(showScrollView);
    }
    void OnClick()
    {

    }
}
