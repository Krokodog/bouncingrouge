﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicOption : MonoBehaviour
{
    public Image icon;
    public Sprite iconMusicOn;
    public Sprite iconMusicOff;
    private bool muteMusic;

    // Start is called before the first frame update
    void Start()
    {
        icon.sprite = iconMusicOn;
        //Bool needed in gamemanager to persist data
    }

    public void ChangeMusicState()
    {
        muteMusic = !muteMusic;
        if (muteMusic)
        {
            icon.sprite = iconMusicOff;
        }
        else
        {
            icon.sprite = iconMusicOn;
        }

    }
    void OnClick()
    {

    }
}
