﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetActiveScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

  
    public void ButtonPlay()
    {
        SceneManager.LoadScene(0);
    }

    public void ButtonShop()
    {
        SceneManager.LoadScene(2);
    }

    public void ButtonExit()
    {
        Debug.Log("Game EXIT");
        Application.Quit();
    }
}
