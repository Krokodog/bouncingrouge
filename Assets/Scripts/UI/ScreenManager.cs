﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ScreenManager : MonoBehaviour {
    public Animator bgAnimator;
    public Animator textAnimator;
    public TextMeshProUGUI curLvl;
    public TextMeshProUGUI nextLvl;


    private int levelToLoad;



    // Use this for initialization
    void Start () {

        curLvl.text = ""+ GameManager.Instance.currentLevel;
        nextLvl.text = "" + (GameManager.Instance.currentLevel+1);
    }
	
	// Update is called once per frame
	void Update () {
        if (bgAnimator.GetCurrentAnimatorStateInfo(0).length > bgAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime)
        {
            GameManager.Instance.gamePaused =true;
        }
        else
        {
            GameManager.Instance.gamePaused = false;
        }	
    }

    public void FadeToNextScreen(int screenIndex)
    {
        levelToLoad = screenIndex;
        bgAnimator.SetTrigger("FadeOut");
        textAnimator.SetTrigger("FadeLevelText");
    }

    public void OnFadeComplete()
     {
        SceneManager.LoadScene(levelToLoad);
     }



}
