﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIBossManager : MonoBehaviour
{
    public Slider lifeBar;
    public TextMeshProUGUI lifeBarText;

    private float bossLife;
    public float currentDamageDealt = 0f;
    private float percent;
    GameManager gm;


    // Use this for initialization
    void Start()
    {
        gm = GameManager.Instance;
        bossLife = gm.bossLife1;
        lifeBarText.text = "" + bossLife;
    }

    // Update is called once per frame
    void Update()
    {

            if (currentDamageDealt >= bossLife)
            {
            //Boss killed
            Debug.Log("Killed dat shit");
            }
 
    }


    public void ReduceBossLife(float defaultVal = 1f)
    {

        if (currentDamageDealt < bossLife - defaultVal)
        {
            currentDamageDealt += defaultVal;
            percent = (bossLife - currentDamageDealt) / bossLife;
            lifeBar.value = Mathf.Lerp(0, 1, percent);
            lifeBarText.text = string.Format("{0:F1}", Mathf.Round((bossLife - currentDamageDealt) * 10f) / 10f);
        }
        else
        {
            currentDamageDealt = bossLife;
        }
    }
    public void IncreaseBossLife(float defaultVal = 1f)
    {
        if (currentDamageDealt <= defaultVal)
        {
            currentDamageDealt = 0;
        }
        else
        {
            currentDamageDealt -= defaultVal;
            percent = (bossLife - currentDamageDealt) / bossLife;
            lifeBar.value = Mathf.Lerp(0, 1, percent);
            lifeBarText.text = string.Format("{0:F1}", Mathf.Round((bossLife - currentDamageDealt) * 10f) / 10f);
        }
    }



}
