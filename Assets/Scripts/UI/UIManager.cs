﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UIManager : MonoBehaviour {

    public Slider timer;
    //public Text timerText;
    public TextMeshProUGUI timerText;


    public int colorScheme = 0;
    private float maxTime;
    public float activeTime = 0f;
    private float percent;
    GameManager gm;

    //public Text textLevel;
    //public Text textMoney;

    public TextMeshProUGUI textLevel;
    public TextMeshProUGUI textMoney;

    // Use this for initialization
    void Start () {
        gm = GameManager.Instance;
        textMoney.text = "" + gm.countMoney;
        textLevel.text = "" + gm.currentLevel;
        maxTime = gm.maxTime;
        activeTime = maxTime;
        timerText.text = "" + maxTime;
    }
	
	// Update is called once per frame
	void Update () {
        if (!gm.gameOver && gm.gameStarted)
        {
            ChangeMoneyText();
            if (activeTime <= maxTime)
            {
                activeTime -= Time.deltaTime;
                percent = (maxTime - activeTime) / maxTime;
                timer.value = Mathf.Lerp(1, 0, percent);
                //timerText.text =  " " + Mathf.Round((maxTime - activeTime) * 10f) / 10f;
                //timerText.text = string.Format("{0:F1}", Mathf.Round((maxTime - activeTime) * 10f) / 10f);
                timerText.text = string.Format("{0:F1}", Mathf.Round((activeTime) * 10f) / 10f);
            }
            else if (!gm.gameOver)
            {
                gm.gameOver = true;
            }
        }
    }

    public void ChangeMoneyText()
    {
        textMoney.text = "" + gm.countMoney;
    }
    public void ChangeLevelText()
    {
        textLevel.text = "" + gm.currentLevel;
    }

    public void DecreaseActiveTime(float defaultVal = 1f)
    {
        Debug.Log("default Val in Decread = " + defaultVal);

        if (activeTime - defaultVal < 0)
        {
            activeTime = 0;
        }
        else
        {
            activeTime -= defaultVal;
        }
    }
    public void IncreaseActiveTime(float defaultVal = 1f)
    {
        Debug.Log("default Val in Increas = " + defaultVal);
        if (activeTime + defaultVal > maxTime)
        {
            activeTime = maxTime;
        }
        else
        {
            activeTime += defaultVal;
        }
    }

    

}
