﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using TMPro;


public class CPalettes
{
    public Color dark;
    public Color c3;
    public Color c2;
    public Color c1;
    public Color bright;

    public CPalettes(Color dark, Color c3, Color c2, Color c1, Color bright)
    {
        this.dark = dark;
        this.c3 = c3;
        this.c2 = c2;
        this.c1 = c1;
        this.bright = bright;
    }
}

public class ColorPaletteManager : MonoBehaviour
{


    //Five color palettes (Dark, C3, C2, C1, Light) static gameobject, enemies need to be colored separately
    /*
     * Dark = Fonts, Background timer, Ball
     * C3 = Player, UI Sepperator
     * C2 = Blocks/Enemy
     * C1 = Timer
     * Light = Background
     */

    // COLOR PALETTES
    public List<CPalettes> allPalettes = new List<CPalettes>();
    Color dark;
    Color cp3;
    Color cp2;
    Color cp1;
    Color bright;
    int[] tmpColor;

    //DARK
    public GameObject ball;
    public GameObject uiBgTimer;
    private GameObject[] uiText;
    private GameObject[] uiImage;

    //C3
    public GameObject player;
    public GameObject uiLevelSeperator;

    //C2 are Enemies, colored in BlackSpawn.cs

    //C1
    public GameObject uiTimer;

    // LIGHT
    public GameObject bg;

    GameObject uimanager;
    UIManager uim;

    UIBossManager uibm;
    void Awake()
    {

        //CPalettes palette1 = new CPalettes;
        string path = "Assets/Resources/ColorPalettes.txt";
        StreamReader sr = new StreamReader(path);

        string line;
        int parsedCurrentColor = 0;
        int palettesLoaded = 0;

        do
        {
            line = sr.ReadLine();
            if (line != null)
            {

                if (parsedCurrentColor % 5 == 0)
                {
                    tmpColor = line.Split(',').Select(System.Int32.Parse).ToArray();
                    dark = new Color(tmpColor[0] / 256f, tmpColor[1] / 256f, tmpColor[2] / 256f);
                    parsedCurrentColor++;
                }
                else if (parsedCurrentColor % 5 == 1)
                {
                    tmpColor = line.Split(',').Select(System.Int32.Parse).ToArray();
                    cp3 = new Color(tmpColor[0] / 256f, tmpColor[1] / 256f, tmpColor[2] / 256f);
                    parsedCurrentColor++;
                }
                else if (parsedCurrentColor % 5 == 2)
                {
                    tmpColor = line.Split(',').Select(System.Int32.Parse).ToArray();
                    cp2 = new Color(tmpColor[0] / 256f, tmpColor[1] / 256f, tmpColor[2] / 256f);
                    parsedCurrentColor++;
                }
                else if (parsedCurrentColor % 5 == 3)
                {
                    tmpColor = line.Split(',').Select(System.Int32.Parse).ToArray();
                    cp1 = new Color(tmpColor[0] / 256f, tmpColor[1] / 256f, tmpColor[2] / 256f);

                    parsedCurrentColor++;
                }
                else if (parsedCurrentColor % 5 == 4)
                {
                    tmpColor = line.Split(',').Select(System.Int32.Parse).ToArray();
                    bright = new Color(tmpColor[0] / 256f, tmpColor[1] / 256f, tmpColor[2] / 256f);
                    CPalettes newCP = new CPalettes(dark, cp1, cp2, cp3, bright);


                    allPalettes.Add(newCP);
                    parsedCurrentColor++;
                    palettesLoaded++;
                }

            }

        } while (line != null);
    }


    void Start()
    {
       
        uimanager = GameObject.FindGameObjectWithTag("uimanager");
        uim = uimanager.GetComponent<UIManager>();
     
        CPalettes currentPalette = allPalettes[uim.colorScheme];         
        
        bg = GameObject.FindGameObjectWithTag("PaletteLight");
        uiText = GameObject.FindGameObjectsWithTag("PaletteDarkText");
        uiImage = GameObject.FindGameObjectsWithTag("PaletteDarkImage");

        foreach (GameObject go in uiText)
        {
            go.GetComponent<TextMeshProUGUI>().color = currentPalette.dark;
        }
        foreach (GameObject go in uiImage)
        {
            go.GetComponent<Image>().color = currentPalette.dark;

        }

        bg.GetComponent<SpriteRenderer>().color = currentPalette.bright;

        ball.GetComponent<SpriteRenderer>().color = currentPalette.dark;

        uiBgTimer.GetComponent<Image>().color = currentPalette.dark;

        player.GetComponent<SpriteRenderer>().color = currentPalette.c3;

        uiLevelSeperator.GetComponent<Image>().color = currentPalette.c3;

        uiTimer.GetComponent<Image>().color = currentPalette.c1;


    }

}
