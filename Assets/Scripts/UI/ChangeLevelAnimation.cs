﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLevelAnimation : MonoBehaviour {

    public RectTransform levelCntHolder;
    private float speed;
    private Vector2 nextLevelTextPosition;
    private bool movingLevelText;


    // Use this for initialization
    void Start () {
        speed = 50;
    }
	
	// Update is called once per frame
	void Update () {
        if (movingLevelText) { 
           levelCntHolder.anchoredPosition = Vector2.MoveTowards(levelCntHolder.anchoredPosition, nextLevelTextPosition, speed * Time.deltaTime);
        }

        if (Vector2.Distance(levelCntHolder.anchoredPosition, nextLevelTextPosition) <= 0.1f)
        {
            movingLevelText = false;
        }
    }

    public void MoveLevelText()
    {
        Vector2 mDir = new Vector2(0, 60f);
        nextLevelTextPosition = levelCntHolder.anchoredPosition + mDir;
        movingLevelText = true;
    }

}
