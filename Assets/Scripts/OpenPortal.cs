﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenPortal : MonoBehaviour {

    public GameObject portal;

    public void OpenPortalToNextLevel()
    {
        GameObject pt = Instantiate(portal, this.transform.position, this.transform.rotation);
        pt.transform.parent = this.transform;
    }
}
