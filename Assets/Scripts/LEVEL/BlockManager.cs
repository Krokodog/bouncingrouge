﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BlockManager : MonoBehaviour
{

    public float hitPoints;
    private SpriteRenderer sprite;

    // Dropable items
    public GameObject coin;
    public GameObject effectOnDead;
    public TextMeshProUGUI life;
    public bool isVenomed;
    private bool venomIsOnCD;

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        hitPoints = Mathf.Ceil(Random.Range(0, hitPoints-0.01f)+0.01f);
        life.text = ""+ hitPoints;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if(isVenomed && !venomIsOnCD)
        {
            venomIsOnCD = true;
            StartCoroutine(ItemVenomCo());
        }

    }

    public void TakeDamage(float damage)
    {
        hitPoints -= damage;
        life.text = "" + hitPoints;

        if (hitPoints <= 0)
        {
            GameManager.Instance.NormalBlockDestroyed();
            if (GameManager.Instance.CreateItemRNG(GameManager.Instance.dropRateCoin))
            {
                Instantiate(coin, this.transform.position, this.transform.rotation);
            }
            this.gameObject.SetActive(false);
            //onDead.Play();

            SoundManager.instance.PlaySingle(SoundManager.instance.blockDestroySource);
            Instantiate(effectOnDead, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject, 2f);
        }
        else
        {
            MakeLighter();
        }

    }
    void MakeLighter()
    {
        Color color = sprite.color;
        float newAlpha = color.a * (1-1/(hitPoints+1));
        sprite.color = new Color(color.r, color.g, color.b, newAlpha);
    }

    private IEnumerator ItemVenomCo()
    {
        
        this.GetComponent<Animator>().SetTrigger("showVenom");
        TakeDamage(GameManager.Instance.venomDamage);
        yield return new WaitForSeconds(GameManager.Instance.venomCD);
        venomIsOnCD = false;
    }
}
