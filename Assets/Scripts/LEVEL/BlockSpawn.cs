﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System;

public enum BlockKind
{
    Breakable,
    Blank,
    Normal
}

[System.Serializable]
public class BlockType
{
    public int x;
    public int y;
    public BlockKind blockKind;
}


public class BlockSpawn : MonoBehaviour
{

    public GameObject block;
    public GameObject smallBlock;

    public int nBlocksHorizontal;
    public int nBlocksVertical;

    private float maxSpawnWidth;
    private float maxSpawnHeight;
    GameObject uimanager;
    UIManager uim;

    /* Level design
     * 0 = empty block
     * 1 = block
     * 2 = unbreakable block
     * 3 = bouncing block
     * 4 = gravity block;
      */

    float newScaleX;
    float newScaleY;
    float newScaleX2;
    float newScaleY2;
    List<string> stringLevelList = new List<string>();
    List<int[]> allLevels = new List<int[]>();

    // Use this for initialization
    void Start()
    {
        uimanager = GameObject.FindGameObjectWithTag("uimanager");
        uim = uimanager.GetComponent<UIManager>();
        maxSpawnWidth = GameObject.FindGameObjectWithTag("DisplaySpawnArea").transform.localScale.x;
        maxSpawnHeight = GameObject.FindGameObjectWithTag("DisplaySpawnArea").transform.localScale.y;

        newScaleX = maxSpawnWidth / nBlocksHorizontal;
        newScaleY = maxSpawnHeight / nBlocksVertical;
        newScaleX2 = maxSpawnWidth * 2 / nBlocksHorizontal;
        newScaleY2 = maxSpawnHeight * 2 / nBlocksVertical;

        // Change block size depending on the number of blocks in horizontal and vertical direction
        smallBlock.transform.localScale = new Vector2(newScaleX, newScaleY);

        ColorPaletteManager cpm = uim.GetComponent<ColorPaletteManager>();
        if (cpm.allPalettes[uim.colorScheme] != null)
        {
            block.GetComponent<SpriteRenderer>().color = cpm.allPalettes[uim.colorScheme].c2;
            smallBlock.GetComponent<SpriteRenderer>().color = cpm.allPalettes[uim.colorScheme].c2;
        }

        //string path = "Assets/Resources/Level1.txt";
        string path = "Assets/Resources/LevelEasy.txt";

        StreamReader sr = new StreamReader(path);

        string line;
        int loadedLevel = 0;
        do
        {
            line = sr.ReadLine();
            if (line != null)
            {
                stringLevelList.Add(line);
                ParseLevelToArray(stringLevelList[loadedLevel]);
                loadedLevel++;
            }

        } while (line != null);

        SetUpLevel();
    }

    void ParseLevelToArray(string currentLevelString)
    {
        // Parsing the text level line to int array, no matter which size
        int[] parsedLevel = currentLevelString.Split(',').Select(Int32.Parse).ToArray();
        allLevels.Add(parsedLevel);

    }


    // Update is called once per frame
    void Update()
    {

    }


    void SetUpLevel()
    {
        int selectRandomLevel = UnityEngine.Random.Range(0, allLevels.Count - 1);
        int[] selectedLevel = allLevels[selectRandomLevel];

        //TESTMODUS last map is just one block
        //int[] selectedLevel = allLevels[allLevels.Count-1];

        Vector2 startPos = new Vector2(transform.position.x - maxSpawnWidth / 2 + smallBlock.transform.localScale.x / 2, transform.position.y + maxSpawnHeight / 2 - smallBlock.transform.localScale.y / 2);
        Vector2 tmpVec;
        float offsetH = 0;
        float offsetV = 0;
        int setCounter = 0;
        

        int[,] helpMatrix = new int[16, 16];

        for (int i = 0; i < nBlocksVertical; i++)
        {

            offsetV = nBlocksVertical - (nBlocksVertical - i);
            offsetV = offsetV * smallBlock.transform.localScale.y;
            for (int j = 0; j < nBlocksHorizontal; j++)
            {

                offsetH = nBlocksHorizontal - (nBlocksHorizontal - j);
                offsetH = offsetH * smallBlock.transform.localScale.x;

                //check if block already in use
                if (selectedLevel[setCounter] == 1 && helpMatrix[i, j] != 1)
                {


                    if (i < nBlocksVertical - 1 && j < nBlocksHorizontal - 1 && selectedLevel[setCounter + 1] == 1 && selectedLevel[setCounter + 16] == 1 && selectedLevel[setCounter + 17] == 1)
                    {
                        tmpVec = new Vector2((startPos.x + offsetH) + smallBlock.transform.localScale.x / 2, (startPos.y - offsetV) - smallBlock.transform.localScale.y / 2);
                        helpMatrix[i, j + 1] = 1;
                        helpMatrix[i + 1, j] = 1;
                        helpMatrix[i + 1, j + 1] = 1;

                        GameObject newBlock = Instantiate(block, tmpVec, transform.rotation);
                        newBlock.transform.localScale = new Vector2(newScaleX2, newScaleY2);
                        newBlock.transform.parent = this.transform;           

                    }
                    else
                    {
                        tmpVec = new Vector2(startPos.x + offsetH, startPos.y - offsetV);
                        GameObject newBlock = Instantiate(smallBlock, tmpVec, transform.rotation);
                        newBlock.transform.parent = this.transform;

                    }
                    GameManager.Instance.countNormalBlocks++;

                }

                helpMatrix[i, j] = 1;

                setCounter++;
            }

        }


    }
}
