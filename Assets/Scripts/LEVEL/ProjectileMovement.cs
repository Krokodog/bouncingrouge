﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{


    private Vector2 startPosition;
    private Vector2 endPosition;
    private Vector2 lastPosition;
    private Vector2 lastlastPosition;

    private float allowedDelta = 0.1f;
    private int allowedDeltaExceededCounter = 0;

    private Vector2 trackPos;


    private Vector2 jumpDirection;


    private float originPos = 0f;
    private float angleSteps = 9f;

    public GameObject playerBar;
    public GameObject movementParticle;

    // Sprite sizes
    public Sprite[] differentBallSizes;

    // Item_Boom
    public GameObject itemBomb;
    public bool boomIsOnCD;

    // Item_Penny
    public GameObject coin;

    // Item_DSpace
    public GameObject dspaceEffect;
    public bool dspaceIsActive;
    public bool dspaceOnCD;
    private GameObject dpsaceEffectHolder;


    private bool ballStopped;
    // Use this for initialization
    void Start()
    {
        GameManager.Instance.curBalls += 1;
        if (!GameManager.Instance.playerInitialized)
        {

            Vector2 randomStart = new Vector2(Random.value * 4 - 2f, this.transform.position.y);
            transform.position = randomStart;
            GetComponent<SpriteRenderer>().sprite = differentBallSizes[GameManager.Instance.playerBallSize];
            GameManager.Instance.playerInitialized = true;

        }
        else
        {
            Vector2 v = new Vector2(1, 0);
            float rotationAngle = 2f * Mathf.PI * Random.Range(0f, 1f);

            Vector2 vRotated = new Vector2(
               (v.x) * Mathf.Cos(rotationAngle) + (v.y) * Mathf.Sin(rotationAngle),
               (v.y) * Mathf.Cos(rotationAngle) - (v.x) * Mathf.Sin(rotationAngle)
            );
            GetComponent<Rigidbody2D>().velocity = vRotated * GameManager.Instance.playerBallSpeed;
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameManager.Instance.gameOver)
        {
            if (!ballStopped)
            {
                this.GetComponent<Rigidbody2D>().isKinematic = true;
                this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                this.GetComponentInChildren<ParticleSystem>().Stop();
                ballStopped = true;
            }
        }
        else
        {

            if (GameManager.Instance.ballIsRunning && (this.gameObject.GetComponent<Rigidbody2D>().velocity == Vector2.zero))
            {
                //  Debug.Log("BALL DOES NOT CHANGE POSITION = " + gameObject.Name);
                FixBlockedBall();
            }

            float curVel = Vector2.SqrMagnitude(gameObject.GetComponent<Rigidbody2D>().velocity);
            float definedMaxVel = Vector2.SqrMagnitude(Vector2.up * GameManager.Instance.playerBallSpeed);
            if (GameManager.Instance.ballIsRunning && (curVel < definedMaxVel * 0.9f))
            {
                // Debug.Log("Velocity of " + gameObject.Name + " is currentyl = " + Vector2.SqrMagnitude(gameObject.GetComponent<Rigidbody2D>().velocity));
                GetComponent<Rigidbody2D>().velocity = gameObject.GetComponent<Rigidbody2D>().velocity.normalized * GameManager.Instance.playerBallSpeed;
            }

        }


    }

    void FixBlockedBall()
    {
        // Debug.Log("Trying to fix");
        Vector2 v = new Vector2(1, 0);

        float rotationAngle = 2f * Mathf.PI * Random.Range(0f, 1f);

        Vector2 vRotated = new Vector2(
           (v.x) * Mathf.Cos(rotationAngle) + (v.y) * Mathf.Sin(rotationAngle),
           (v.y) * Mathf.Cos(rotationAngle) - (v.x) * Mathf.Sin(rotationAngle)
        );
        GetComponent<Rigidbody2D>().velocity = vRotated * GameManager.Instance.playerBallSpeed;
    }

    Vector2 getDirection(Vector2 startPosition, Vector2 endPosition)
    {
        Vector2 direction = startPosition - endPosition;
        return direction;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        SoundManager.instance.PlaySingle(SoundManager.instance.collisionBallSource);

        // Check if ball just stays in the same direction
        if (lastlastPosition == col.GetContact(0).point)
        {

            // Debug.Log("This one is stucked " + gameObject.Name );
            FixBlockedBall();
            allowedDeltaExceededCounter = 0;
            Debug.Log("firstIF lastlast=");
        }

        if (Mathf.Abs(lastlastPosition.y - col.GetContact(0).point.y) < allowedDelta)
        {
            allowedDeltaExceededCounter += 1;
        }
        else if(allowedDeltaExceededCounter >= 5)
        {
            FixBlockedBall();
            allowedDeltaExceededCounter = 0;
            Debug.Log("Second FIX");
        }

        if (col.gameObject.tag == "Level")
        {
            Vector2 collisionPoint = col.GetContact(0).point;
            Vector2 inNormal = col.GetContact(0).normal;
            Vector2 inDirection = collisionPoint - lastPosition;

            // Debug.Log("Angle of this is = " + Vector2.Angle(inDirection, inNormal));
            if (lastPosition != collisionPoint)
            {
                if (Vector2.Angle(inDirection, inNormal) >= 172)
                {
                    inDirection = inDirection + new Vector2(0, Random.Range(0.01f, 0.04f));
                }
                else
                {
                    ReflectJump(inDirection, inNormal);
                }
                // Debug.Log("Angle of this is = " +Vector2.Angle(inDirection, inNormal));

            }

            lastlastPosition = lastPosition;
            lastPosition = collisionPoint;

        }

        if (col.gameObject.tag == "LevelDead")
        {
            if (!GameManager.Instance.gameOver)
            {
                GameManager.Instance.gameOver = true;

            }
        }

        if (col.gameObject.tag == "Enemy")
        {
            Vector2 collisionPoint = col.GetContact(0).point;
            Vector2 inDirection = collisionPoint - lastPosition;

            if (!dspaceIsActive)
            {
                Vector2 inNormal = col.GetContact(0).normal;
                lastlastPosition = lastPosition;
                lastPosition = collisionPoint;

                // Check if reflection is not zero!
                if (inDirection != Vector2.zero)
                {
                    ReflectJump(inDirection, inNormal);
                }

                // Check if venom is collected and check if venomized
                if (GameManager.Instance.venomCollected && !GameManager.Instance.gameOver)
                {
                    if (GameManager.Instance.CreateItemRNG(GameManager.Instance.dropRateVenom) && !col.gameObject.GetComponent<BlockManager>().isVenomed)
                    {

                        col.gameObject.GetComponent<BlockManager>().isVenomed = true;
                    }
                }
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = inDirection.normalized * GameManager.Instance.playerBallSpeed;
            }
            col.gameObject.GetComponent<BlockManager>().TakeDamage(GameManager.Instance.playerDamage);

        }

        /* Special case: player can influence ball direction by collision point
        * transform shifted collision point to origin
        * calculate distance to origin
        * calculate the new direction vector by mapping the distance to an vector with a specific angle
        * e.g hit point at 0.5f length causing an up reflection, |0.25f| causing 45° reflection etc.
        * following mapping table needs to be calculated for hit boundary else normal reflection: 
        *0.00f - 0.05f => 0°
        *0.05f - 0.15f => 22.5°
        *0.15f - 0.35f => 45°
        *0.35f - 0.45f => 67.5°
        *0.45f - 0.55f => 90°
        *0.55f - 0.65f => 67,5°
        *0.65f - 0.85f => 45°
        *0.85f - 0.95f => 22.5°
        *0.95f - 1.00f => 0°
        */


        if (col.gameObject.tag == "Player")
        {

            // must be == 0
            if (GameManager.Instance.countNormalBlocks == 0 && !GameManager.Instance.isBossRoom)
            {
                //For test scenario disabled
                this.gameObject.SetActive(false);

                //Set Game to not started
                GameManager.Instance.gameStarted = false;
                GameManager.Instance.playerInitialized = false;
                GameManager.Instance.ballIsRunning = false;
                // If level mod 5 = 0 ->Bossroom else normal level
                if(GameManager.Instance.currentLevel %  5 != 0)
                {
                    GameObject.FindGameObjectWithTag("LevelChanger").GetComponent<ScreenManager>().FadeToNextScreen(0);
                }
                else
                {
                    GameObject.FindGameObjectWithTag("LevelChanger").GetComponent<ScreenManager>().FadeToNextScreen(3);
                }
                


            }
            else
            {
                float playerXSize = col.gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
                Vector2 collisionPoint = col.GetContact(0).point;

                // transform collision point to a distance in perspective to origin 0 and only positive, therefore range 0 to 1
                float distanceToOrigin = (originPos - (col.transform.position.x - collisionPoint.x) + playerXSize / 2f) / playerXSize;


                if ((distanceToOrigin <= 0f) || (distanceToOrigin >= 1f))
                {
                    Vector2 inNormal = col.GetContact(0).normal;
                    Vector2 inDirection = collisionPoint - lastPosition;
                    lastlastPosition = lastPosition;
                    lastPosition = collisionPoint;
                    ReflectJump(inDirection, inNormal);
                }
                else
                {
                    // Get positive range of distanceToOrigin and map value to an angle of a specific step
                    float reflectionAngle = Mathf.Round(distanceToOrigin * angleSteps) * 180f / angleSteps;
                    //Debug.Log(reflectionAngle);

                    // If corner point collision for left and right or really steep angle ~5°
                    if (reflectionAngle == 0f)
                    {
                        reflectionAngle += 15;
                    }
                    else if (reflectionAngle == 180f)
                    {
                        reflectionAngle -= 15;
                    }
                    Vector2 newDirection = Vector2.zero;
                    Vector2 inDirection = collisionPoint - lastPosition;
                    // Debug.Log(inDirection.y);
                    if (inDirection.y <= 0)
                    {
                        newDirection = Vector2.right;
                    }
                    else
                    {
                        newDirection = Vector2.left;
                    }
                    newDirection = Quaternion.Euler(0, 0, reflectionAngle) * newDirection;
                    newDirection.x = newDirection.x * -1f;
                    lastPosition = collisionPoint;
                    GetComponent<Rigidbody2D>().velocity = newDirection * GameManager.Instance.playerBallSpeed;
                }

            }

        }

        if (GameManager.Instance.boomCollected && !GameManager.Instance.gameOver)
        {
            if (GameManager.Instance.CreateItemRNG(GameManager.Instance.dropRateBomb) && !boomIsOnCD)
            {

                boomIsOnCD = true;
                StartCoroutine(ItemBoomCo());
                StartCoroutine(ItemBoomTimerCo());
            }
        }

        if (GameManager.Instance.pennyCollected && !GameManager.Instance.gameOver)
        {
            if (GameManager.Instance.CreateItemRNG(GameManager.Instance.dropRatePenny))
            {
                Instantiate(coin, this.transform.position, this.transform.rotation);
            }
        }

        if (GameManager.Instance.dspaceCollected && !GameManager.Instance.gameOver)
        {
            if (GameManager.Instance.CreateItemRNG(GameManager.Instance.dropRateDSpace) && !dspaceIsActive && !dspaceOnCD)
            {
                dspaceIsActive = true;
                dpsaceEffectHolder = Instantiate(dspaceEffect, this.transform.position, this.transform.rotation);
                dpsaceEffectHolder.transform.parent = this.transform;
                movementParticle.SetActive(false);
                StartCoroutine(ItemDSpaceCo());
            }
        }

        if (col.gameObject.tag == "Boss")
        {
            Vector2 collisionPoint = col.GetContact(0).point;
            Vector2 inDirection = collisionPoint - lastPosition;

            if (!dspaceIsActive)
            {
                Vector2 inNormal = col.GetContact(0).normal;
                lastlastPosition = lastPosition;
                lastPosition = collisionPoint;

                // Check if reflection is not zero!
                if (inDirection != Vector2.zero)
                {
                    ReflectJump(inDirection, inNormal);
                }

                // Check if venom is collected and check if venomized
                if (GameManager.Instance.venomCollected && !GameManager.Instance.gameOver)
                {
                    if (GameManager.Instance.CreateItemRNG(GameManager.Instance.dropRateVenom) && !col.gameObject.GetComponent<BlockManager>().isVenomed)
                    {

                        col.gameObject.GetComponent<BlockManager>().isVenomed = true;
                    }
                }
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = inDirection.normalized * GameManager.Instance.playerBallSpeed;
            }
            col.gameObject.GetComponent<Boss1Manager>().TakeDamage(GameManager.Instance.playerDamage);

        }

        if (col.gameObject.tag == "BossEnemy")
        {
            Vector2 collisionPoint = col.GetContact(0).point;
            Vector2 inDirection = collisionPoint - lastPosition;

            if (!dspaceIsActive)
            {
                Vector2 inNormal = col.GetContact(0).normal;
                lastlastPosition = lastPosition;
                lastPosition = collisionPoint;

                // Check if reflection is not zero!
                if (inDirection != Vector2.zero)
                {
                    ReflectJump(inDirection, inNormal);
                }

                // Check if venom is collected and check if venomized
                if (GameManager.Instance.venomCollected && !GameManager.Instance.gameOver)
                {
                    if (GameManager.Instance.CreateItemRNG(GameManager.Instance.dropRateVenom) && !col.gameObject.GetComponent<BlockManager>().isVenomed)
                    {

                        col.gameObject.GetComponent<BlockManager>().isVenomed = true;
                    }
                }
            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = inDirection.normalized * GameManager.Instance.playerBallSpeed;
            }
            col.gameObject.GetComponent<Boss1Block>().TakeDamage(GameManager.Instance.playerDamage);

        }
    }

    void ReflectJump(Vector2 inDirection, Vector2 inNormal)
    {
        Vector2 reflection = Vector2.Reflect(inDirection, inNormal).normalized;
        GetComponent<Rigidbody2D>().velocity = reflection * GameManager.Instance.playerBallSpeed;
    }

    public void StartBallMovement()
    {
        GetComponent<Rigidbody2D>().velocity = (playerBar.transform.position - this.transform.position).normalized * GameManager.Instance.playerBallSpeed;
        GameManager.Instance.ballIsRunning = true;
        this.GetComponentInChildren<ParticleSystem>().Play();
    }

    public void ChangeBallSize()
    {
        GetComponent<SpriteRenderer>().sprite = differentBallSizes[GameManager.Instance.playerBallSize];
        Destroy(GetComponent<CircleCollider2D>());
        gameObject.AddComponent<CircleCollider2D>();
    }

    private IEnumerator ItemBoomCo()
    {
        yield return new WaitForSeconds(GameManager.Instance.boomCD);
        boomIsOnCD = false;
    }

    private IEnumerator ItemBoomTimerCo()
    {
        float randBoomTime = Random.Range(2, 4);
        yield return new WaitForSeconds(randBoomTime);
        Instantiate(itemBomb, this.transform.position, this.transform.rotation);
    }

    private IEnumerator ItemDSpaceCo()
    {
        yield return new WaitForSeconds(GameManager.Instance.dspaceActiveTime);
        dspaceIsActive = false;
        dspaceOnCD = true;
        movementParticle.SetActive(true);
        Destroy(dpsaceEffectHolder);
        StartCoroutine(ItemDSpaceCooldownCo());
    }
    private IEnumerator ItemDSpaceCooldownCo()
    {
        yield return new WaitForSeconds(GameManager.Instance.dspaceCD);
        dspaceOnCD = false;
    }

    //point[1] = point[0] +(point[2] -point[0])/2 +Vector3.up*5.0f;

    //float count = 0.0f;

    // void Update()
    // {
    //     if (count < 1.0f)
    //     {
    //          count += 1.0f * Time.deltaTime;
    //
    //         Vector3 m1 = Vector3.Lerp(point[0], point[1], count);
    //          Vector3 m2 = Vector3.Lerp(point[1], point[2], count);
    //         myObject.position = Vector3.Lerp(m1, m2, count);
    //      }
    //   }
}
