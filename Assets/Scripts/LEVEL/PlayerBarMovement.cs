using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerBarMovement : MonoBehaviour
{

    // Boundary variables
    private float boundOffsetHorizontal;
    private float camHeight;
    private float camWidth;


    public ActiveItemManager activeItemManager;
    // Use this for initialization
    void Start()
    {
        camHeight = 2f * Camera.main.orthographicSize;
        camWidth = camHeight * Camera.main.aspect;
        activeItemManager = this.gameObject.AddComponent<ActiveItemManager>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!GameManager.Instance.gameOver && !GameManager.Instance.gamePaused)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                if (!GameManager.Instance.gameStarted)
                {
                    GameManager.Instance.startGame();
                }
                this.transform.position = this.transform.position + Vector3.left * GameManager.Instance.playerSpeed * Time.deltaTime;
                CheckValidMovement("left");
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                if (!GameManager.Instance.gameStarted)
                {
                    GameManager.Instance.startGame();
                }
                this.transform.position = this.transform.position + Vector3.right * GameManager.Instance.playerSpeed * Time.deltaTime;
                CheckValidMovement("right");
            }
            if (Input.GetMouseButtonDown(0))
            {
                //Trigger Event of currActiveItem
                activeItemManager.callTriggerActiveItem();

            }
        }
    }

    void CheckValidMovement(string direction)
    {
        float playerXSize = this.gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        boundOffsetHorizontal = playerXSize / 2f;
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -camWidth / 2f + boundOffsetHorizontal, camWidth / 2f - boundOffsetHorizontal), transform.position.y, 0.0f);
    }

    // Create methods for items

    public void ItemEvaluation(string itemName)
    {
       // Debug.Log(itemName);
        switch (itemName)
        {
            case "Coin":
                SoundManager.instance.PlaySingle(SoundManager.instance.coinSource);
                GameManager.Instance.IncreaseMoney();
                break;
            case "SpeedMinus":                
                GameManager.Instance.DecreaseBallSpeed();
                break;
            case "SpeedPlus":
                GameManager.Instance.IncreaseBallSpeed();
                break;
            case "TimeMinus":
                GameManager.Instance.DecreaseTime(GameManager.Instance.general_ItemTimeValue);
                break;
            case "TimePlus":
                GameManager.Instance.IncreaseTime(GameManager.Instance.general_ItemTimeValue);
                break;
            case "CoinMinus":
                GameManager.Instance.DecreaseMoney();
                break;
            case "GreedIsGood":
                GameManager.Instance.Item_GreedIsGood();
                break;
            case "Tumor":
                GameManager.Instance.Item_Tumor();
                break;
            case "Supersonic":
                GameManager.Instance.Item_Supersonic();
                break;
            case "Halftime":
                GameManager.Instance.Item_Halftime();
                break;
            case "ShinyBerry":
                GameManager.Instance.Item_ShinyBerry();
                break;
            case "Minimum":
                GameManager.Instance.Item_Minimum();
                break;
            case "Bell":
                GameManager.Instance.Item_Bell();
                break;
            case "ImFat":
                GameManager.Instance.Item_ImFat();
                break;
            case "LuckyMouse":
                GameManager.Instance.Item_LuckyMouse();
                break;
            case "AllIn":
                GameManager.Instance.Item_AllIn();
                break;
            case "Boom":
                GameManager.Instance.Item_Boom();
                break;
            case "Venom":
                GameManager.Instance.Item_Venom();
                break;
            case "Penny":
                GameManager.Instance.Item_Penny();
                break;
            case "DSpace":
                GameManager.Instance.Item_DSpace();
                break;
            case "ParallelUniverse":
                GameManager.Instance.Item_ParallelUniverse();
                break;
            case "Clone":
                GameManager.Instance.Item_Clone();
                break;
            case "M_Gun":
                activeItemManager.setActiveItem(itemName);
                break;
            case "Lazer":
                activeItemManager.setActiveItem(itemName);
                break;
            default:
                break;
        }

   
    }


}
