﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Boss1Manager : MonoBehaviour
{
    //Stats
    private float hitPoints = 30;
    private float curHitPoints;
    private int cPlaceBlocks = 3;
    private int cPlaceBlocksPhase2 = 6;
    private int cdAttack = 12;
    private int cdAttackPhase2 = 10;
    private int chanceToAttackOnHit = 35;
    private bool bossIsAttacking;
    private int coinLoot = 20;

    public bool bossPhase2;

    public Animator bossAnim;
    public Sprite awakeSprite;
    private bool isBossAwake;


    // Loot drop
    public GameObject coin;
    //public GameObject item;


    //UI    
    public GameObject effectOnDead;
    public TextMeshProUGUI life;
    public Slider lifeSlider;


    //Track spawned blocks
    public GameObject bossBlock;
    public GameObject[] bossBlockHolder;
    private bool blocksNotPlacable;
    private float blockLife;

    //public bool isVenomed;
    //private bool venomIsOnCD;


    void Start()
    {
        GameManager.Instance.isBossRoom = true;
        curHitPoints = hitPoints;
        life.text = "" + hitPoints;
        lifeSlider.maxValue = hitPoints;
        lifeSlider.value = lifeSlider.maxValue;

    }


    // Update is called once per frame
    void FixedUpdate()
    {
        /*if (isVenomed && !venomIsOnCD)
        {
            venomIsOnCD = true;
            StartCoroutine(ItemVenomCo());
        }
        */

    }

    public void TakeDamage(float damage)
    {
        if (isBossAwake)
        {
            curHitPoints -= damage;
            

            if (curHitPoints <= 0)
            {
                lifeSlider.value = 0;
                life.text = "" + curHitPoints;
               // if (GameManager.Instance.CreateItemRNG(GameManager.Instance.dropRateCoin))
               // {
                    for (int i = 0; i < Random.Range(coinLoot-5, coinLoot+6); i++)
                    {
                        GameObject go = Instantiate(coin, this.transform.position, this.transform.rotation);
                        Vector2 random = new Vector2(1.25f * Random.Range(0f, 1f), 1.25f * Random.Range(0f, 1f));
                        go.transform.position = (Vector2)go.transform.position + random;
                    }

              //  }
                this.gameObject.SetActive(false);
                //onDead.Play();

                SoundManager.instance.PlaySingle(SoundManager.instance.blockDestroySource);
                Instantiate(effectOnDead, this.transform.position, this.transform.rotation);
                GameManager.Instance.isBossRoom = false;
                GameManager.Instance.currentLevel++;
                Destroy(this.gameObject, 2f);

            }
            else
            {
                lifeSlider.value = curHitPoints;
                life.text = "" + curHitPoints;
                if (GameManager.Instance.CreateItemRNG(chanceToAttackOnHit) && !bossIsAttacking)
                {
                    StartCoroutine(BossAttackCDCo());
                    bossIsAttacking = true;
                }

            }

            if (curHitPoints <= hitPoints / 2 && !bossPhase2)
            {
                Debug.Log("Phase 2 Active");
                bossAnim.SetTrigger("phase2");
                cdAttack = cdAttackPhase2;
                bossPhase2 = true;
                cPlaceBlocks = cPlaceBlocksPhase2;
            }


        }
        else
        {
            isBossAwake = true;
            bossAnim.SetTrigger("awakeBoss");
            GetComponent<SpriteRenderer>().sprite = awakeSprite;
        }


    }


    public void ShakeCamera()
    {
        Camera.main.GetComponent<Shake>().CamShake();
    }

    public void TriggerAttack()
    {
        bossAnim.SetTrigger("attack");
    }

    private IEnumerator ItemVenomCo()
    {

        this.GetComponent<Animator>().SetTrigger("showVenom");
        TakeDamage(GameManager.Instance.venomDamage);
        yield return new WaitForSeconds(GameManager.Instance.venomCD);
        //  venomIsOnCD = false;
    }

    private IEnumerator BossAttackCDCo()
    {
        bossAnim.SetTrigger("attack");
        yield return new WaitForSeconds(cdAttack);
        bossIsAttacking = false;
    }

    public void PlaceBlocks()
    {
        List<int> blocksSpawns = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        for (int i = 0; i < blocksSpawns.Count; i++)
        {
            int temp = blocksSpawns[i];
            int randomIndex = Random.Range(i, blocksSpawns.Count);
            blocksSpawns[i] = blocksSpawns[randomIndex];
            blocksSpawns[randomIndex] = temp;
        }

        for (int j = 0; j < cPlaceBlocks; j++)
        {
            for (int k = 0; k < blocksSpawns.Count; k++)
            {
                if (bossBlockHolder[blocksSpawns[k]].transform.childCount == 0)
                {
                    GameObject newBossBlock = Instantiate(bossBlock, bossBlockHolder[blocksSpawns[k]].transform.position, transform.rotation);
                    newBossBlock.transform.parent = bossBlockHolder[blocksSpawns[k]].transform;

                    break;
                }

            }

        }
    }


}
