﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Boss1Block : MonoBehaviour
{
    // Phase 1 2hp, Phase2 4hp
    private float hitPoints = 2;
    private float curHitPoints;

    private SpriteRenderer sprite;

    private GameObject boss1;

    // Dropable items
    public GameObject effectOnDead;
    public TextMeshProUGUI life;
    public bool isVenomed;
    private bool venomIsOnCD;

    void Start()
    {
        boss1 = GameObject.FindGameObjectWithTag("Boss");
        sprite = GetComponent<SpriteRenderer>();

        if (boss1.GetComponent<Boss1Manager>().bossPhase2)
        {
            hitPoints = Mathf.Floor(Random.Range(hitPoints - 1, hitPoints + 1) + 1f) * 2;
        }
        else
        {
            hitPoints = Mathf.Floor(Random.Range(hitPoints - 1, hitPoints + 1) + 1f);
        }
        life.text = "" + hitPoints;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (isVenomed && !venomIsOnCD)
        {
            venomIsOnCD = true;
            StartCoroutine(ItemVenomCo());
        }

    }

    public void TakeDamage(float damage)
    {
        hitPoints -= damage;
        life.text = "" + hitPoints;

        if (hitPoints <= 0)
        {
            this.gameObject.SetActive(false);
            //onDead.Play();

            SoundManager.instance.PlaySingle(SoundManager.instance.blockDestroySource);
            Instantiate(effectOnDead, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject, 2f);
        }
        else
        {
            MakeLighter();
        }

    }
    void MakeLighter()
    {
        Color color = sprite.color;
        float newAlpha = color.a * (1 - 1 / (hitPoints + 1));
        sprite.color = new Color(color.r, color.g, color.b, newAlpha);
    }

    private IEnumerator ItemVenomCo()
    {

        this.GetComponent<Animator>().SetTrigger("showVenom");
        TakeDamage(GameManager.Instance.venomDamage);
        yield return new WaitForSeconds(GameManager.Instance.venomCD);
        venomIsOnCD = false;
    }
}
