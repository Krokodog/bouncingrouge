﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;
    public StatManager statManager;

    public bool gameOver;
    public bool gameStarted;
    public bool gamePaused;
    public bool isBossRoom;

    //NEED TO BE RESET ON LEVELEND!
    public bool playerInitialized;
    public bool ballIsRunning;
    //Lui ist ein Italiener
    //Level balls active
    public int curBalls;
    public bool checkBallsOnCD;
    //Level time
    public float maxTime;
    //Boss1 Life
    public float bossLife1;

    //Current money // drop rate coin
    public int countMoney;
    public int dropRateCoin = 20;

    //Killable blocks alive
    public int countNormalBlocks;

    //Current level
    public int currentLevel;

    //Player stats
    public int playerHealth;
    public float playerDamage;

 

    // 1 Luck = 5 % chance, increases drop chance and passive rates
    // playerLuck max 20
    // playerLuck min 0
    public int playerLuck;

    // playerSize max 6
    // playerSize max 1
    public int playerSize;

    // playerBallSpeed max 13
    // playerBallSpeed min 5
    public float playerSpeed;
    private float playerSpeedMax = 13;

    // playerBallSpeed max 9 (13)
    // playerBallSpeed min 4
    public float playerBallSpeed;
    private float playerBallSpeedMax = 9;

    // playerBallSize min 1 max 3;
    public int playerBallSize;

    //Item falling speed
    public float itemSpeed;
   
    //Item_GreedIsGood
    private bool greedIsGood;

    //Item_Minimum
    private bool minimum;
    private float minimumTimeMultiplicator = 4f;

    //Item_Boom
    public int bombDamage;
    public bool boomCollected;
    public float boomCD;
    public int dropRateBomb = 5;

    //Item_Venom
    public int venomDamage;
    public bool venomCollected;
    public float venomCD;
    public int dropRateVenom = 15;

    //Item_Penny
    public bool pennyCollected;
    public int dropRatePenny = 5;

    //Item_DSpce
    public bool dspaceCollected;
    public float dspaceActiveTime;
    public int dropRateDSpace = 5;
    public float dspaceCD;

    //Item_ParallelUniverse
    public bool parallelUniverseCollected;

    //Item_Clone
    public bool cloneCollected;

    //Item_Clone and Item_parallelUniverseCollected
    public GameObject playerBallTemplate;

    //Item_Active_General
    public bool generalActiveItemCollected;
    public string currentActiveItem;
    public GameObject mGunLeftSpawn;
    public GameObject mGunRightSpawn;
    public GameObject mGunProjectileLeft;
    public GameObject mGunProjectileRight;
    public GameObject laserProjectile;
    public GameObject laserSpawn;


    //Item_Active_MGUN
    public float mGunDamage;
    public int mGunAmmunition;
    private int currentmGunAmmunition;
    public float mGunReloadCD;
    public bool mGunReloading;


    //Item_Active_Lazer
    public float lazerDamage;
    public int lazerAmmunition;
    private int currentLazerAmmunition;
    public float lazerReloadCD;
    public bool lazerReloading;
    

    // value of debuff/buff timer
    public float general_ItemTimeValue = 10f;


    // Active weapons
    public GameObject cannonWeapon;
    public GameObject laserWeapon;
    public GameObject activeItemCanvas;

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        statManager = new StatManager();
        statManager.LoadStatsFromLocalStorage();

    }

    // Use this for initialization
    void Start()
    {
        // Needs to be activatet by player on first movement
        gameStarted = false;
        activeItemCanvas.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

       // if (!checkBallsOnCD)
        //{
            //checkBallsOnCD = true;
           // StartCoroutine(CheckBallsCo());
        //}
        
       
    }

    private IEnumerator CheckBallsCo()
    {
        Debug.Log("Currently we have so many Balls = " + curBalls);
        yield return new WaitForSeconds(5);
        checkBallsOnCD = false;
    }

    public void startGame()
    {
        gameStarted = true;
        GameObject startBall = GameObject.FindGameObjectWithTag("PlayerProjectile");
        startBall.GetComponent<ProjectileMovement>().StartBallMovement();
    }

    public void NormalBlockDestroyed()
    {
        countNormalBlocks--;
        if (countNormalBlocks == 0) {

            Debug.Log("Level Finished");
            GameObject.FindGameObjectWithTag("Portal").GetComponent<OpenPortal>().OpenPortalToNextLevel();
            currentLevel++;
        }
 
    }

    // GENERAL STATS METHODS
    public void IncreaseBallSpeed(int defaultVal = 1)
    {
        // 9 is defined as max speed without items, global max is 13
        if ((playerBallSpeed + defaultVal) <= playerBallSpeedMax)
        {
            playerBallSpeed += defaultVal;
        }
        else
        {
            playerBallSpeed = playerBallSpeedMax;
        }
    }

    public void DecreaseBallSpeed(int defaultVal = 1)
    {
        // 4 is defined as min speed
        if ((playerBallSpeed-defaultVal) >= 4)
        {
            playerBallSpeed -= defaultVal;
        }
        else
        {
            playerBallSpeed = 4;
        }
    }

    public void IncreasePlayerSpeed(int defaultVal = 1)
    {
        // player speed global max is 13
        if ((playerSpeed + defaultVal) <= playerSpeedMax)
        {
            playerSpeed += defaultVal;
        }
        else
        {
            playerSpeed = playerSpeedMax;
        }
    }

    public void DecreasePlayerSpeed(int defaultVal = 1)
    {
        // 5 is defined as min speed
        if ((playerSpeed - defaultVal) >= 5)
        {
            playerSpeed -= defaultVal;
        }
        else
        {
            playerSpeed = 5;
        }
    }
/*
    public void IncreasePlayerSize(int defaultVal = 1)
    {
        // 6 is defined as max size
        if ((playerSize + defaultVal) <= 6)
        {
            playerSize += defaultVal;
        }
        else
        {
            playerSize = 6;
        }
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBarMovement>().ChangePlayerSize();
    }

    public void DecreasePlayerSize(int defaultVal = 1)
    {
        // 1 is defined as min size
        if ((playerSize - defaultVal) >= 1)
        {
            playerSize -= defaultVal;
        }
        else
        {
            playerSize = 1;
        }
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBarMovement>().ChangePlayerSize();
    }
*/
    // Ball size
    public void IncreaseBallSize(int defaultVal = 1)
    {
        // 3 is defined as max size
        if ((playerBallSize + defaultVal) <= 3)
        {
            playerBallSize += defaultVal;
            //GameObject.FindGameObjectWithTag("PlayerProjectile").GetComponent<ProjectileMovement>().ChangeBallSize();
            GameObject[] getCurrentBalls = GameObject.FindGameObjectsWithTag("PlayerProjectile");

            foreach (var ball in getCurrentBalls)
            {
                ball.GetComponent<ProjectileMovement>().ChangeBallSize();
            }
        }
        else
        {
            playerBallSize = 3;
        }
    }

    public void DecreaseBallSize(int defaultVal = 1)
    {
        // 0 is defined as min size
        if ((playerBallSize - defaultVal) >= 0)
        {
            playerBallSize -= defaultVal;
            GameObject[] getCurrentBalls = GameObject.FindGameObjectsWithTag("PlayerProjectile");

            foreach (var ball in getCurrentBalls)
            {
                ball.GetComponent<ProjectileMovement>().ChangeBallSize();
            }
        }
        else
        {
            playerBallSize = 0;
        }
    }

    public void IncreaseTime(float defaultVal = 1f)
    {
        GameObject.FindGameObjectWithTag("uimanager").GetComponent<UIManager>().IncreaseActiveTime(defaultVal);
    }

    public void DecreaseTime(float defaultVal = 1f)
    {
        GameObject.FindGameObjectWithTag("uimanager").GetComponent<UIManager>().DecreaseActiveTime(defaultVal);
    }

    public void IncreaseMoney(int defaultVal = 1)
    {
        if (!minimum)
        {
            if (!greedIsGood)
            {
                countMoney += defaultVal;
            }
            else
            {
                countMoney += defaultVal * 2;
                DecreaseTime(defaultVal * 2);
            }
        }
        else
        {
            GameObject.FindGameObjectWithTag("uimanager").GetComponent<UIManager>().IncreaseActiveTime(minimumTimeMultiplicator);
        }
       // GameObject.FindGameObjectWithTag("uimanager").GetComponent<UIManager>().ChangeMoneyText();
        
    }

    public void DecreaseMoney()
    {
        if (countMoney > 0)
        {
            int randomAmount = Random.Range(1, Mathf.Max(1, (int)Mathf.Ceil(countMoney / 2)));
            countMoney -= randomAmount;
            GameObject.FindGameObjectWithTag("uimanager").GetComponent<UIManager>().ChangeMoneyText();
        }
    }

    // Drop chance generator
    public bool CreateItemRNG(int dropChance)
    {
        int rand = Random.Range(0, 100);
        if (rand < dropChance)
        {
            return true;
        }

        return false;
    }

    // STATS ITEMS
    public void Item_GreedIsGood()
    {
        // Double gold income, every coin looses 2 seconds
        greedIsGood = true;
    }

    public void Item_Tumor()
    {
        // Damage is doubled, Speed reduced by 1
        playerDamage = playerDamage * 2;
        DecreaseBallSpeed(2);
    }

    public void Item_Supersonic()
    {
        // Max speed, Size reduced by 2
        IncreaseBallSpeed(100);
        IncreasePlayerSpeed(100);
        playerBallSpeedMax = 15;

        //Balance size
        //IncreaseSize(1);
    }

    public void Item_Halftime()
    {
        // Max speed, Size increased by 3, Time reduced by 50%
        IncreaseBallSpeed(100);
        //IncreasePlayerSize(3);
        maxTime = maxTime / 2f;
    }

    public void Item_ShinyBerry()
    {
        // Add double amount of current coins
        countMoney += 5;
        IncreaseMoney((int)Mathf.Floor(countMoney/2));

    }

    public void Item_Minimum()
    {
        // Player size min, Ball speed min, no money, each coin +2s time
       // DecreasePlayerSize(100);
        DecreaseBallSpeed(100);
        minimum = true;

    }

    public void Item_Bell()
    {
        // Luk +3, Size +1;
        playerLuck += 3;
        //IncreasePlayerSize(1);
        IncreaseBallSize(1);
    }

    public void Item_ImFat()
    {
        // Ball size +1, Player speed -2
        IncreaseBallSize(1);
        DecreasePlayerSpeed(2);

    }

    public void Item_LuckyMouse()
    {
        // Luck +2, Dmg +1, Player size +1
        playerLuck += 2;
        playerDamage += 1;
       // IncreasePlayerSize(1);

    }

    public void Item_AllIn()
    {
        // Luck +3, size min , speed min, 50g
        playerLuck += 3;
       // DecreasePlayerSize(100);
        DecreaseBallSpeed(100);
        IncreaseMoney(50);

    }

    // PASSIVE Items
    public void Item_Boom()
    {
        //Boom is collected;
        boomCollected = true;
    }

    public void Item_Venom()
    {
        //Venom is collected;
        venomCollected = true;
    }

    public void Item_Penny()
    {
        //Penny is collected;
        pennyCollected = true;
    }

    public void Item_DSpace()
    {
        //DSpace is collected;
        dspaceCollected = true;

    }

    public void Item_ParallelUniverse()
    {
        //parallelUniverse is collected, copy player ball, change size -1, dmg/2;
        parallelUniverseCollected = true;
        playerDamage = playerDamage / 2f;
        DecreaseBallSize(1);
        GameObject[] getCurrentBalls = GameObject.FindGameObjectsWithTag("PlayerProjectile");

        foreach (var ball in getCurrentBalls)
        {
           GameObject go= Instantiate(playerBallTemplate, ball.transform.position, ball.transform.rotation);
            go.name = go.name + (int) Random.Range(0, 10000);
        }

        

    }

    public void Item_Clone()
    {
        //clone is collected, copy playerBall
        cloneCollected = true;
        GameObject[] getCurrentBalls = GameObject.FindGameObjectsWithTag("PlayerProjectile");

        foreach (var ball in getCurrentBalls)
        {
            GameObject go = Instantiate(playerBallTemplate, ball.transform.position, ball.transform.rotation);
            go.name = go.name + (int)Random.Range(0, 10000);
        }

    }

    public void Item_MGun()
    {
        //mgun is collected, activated weapon and active item slot
        generalActiveItemCollected = true;
        currentActiveItem = "Item_MGun";
        activeItemCanvas.SetActive(true);
        cannonWeapon.SetActive(true);
        currentmGunAmmunition = mGunAmmunition;
        GameObject.FindGameObjectWithTag("ActiveItemSlot").GetComponent<ActiveItemManager>().SetCurrentActiveItem(0);
        TextMeshProUGUI test = GameObject.FindGameObjectWithTag("AmmunitionTag").GetComponent<TextMeshProUGUI>();
        test.SetText("" + currentmGunAmmunition);   
    }

    public void MGunShoot()
    {
        if (generalActiveItemCollected && !mGunReloading)
        {
            if (currentmGunAmmunition > 1)
            {
                Instantiate(mGunProjectileLeft, mGunLeftSpawn.transform.position, mGunLeftSpawn.transform.rotation);
                Instantiate(mGunProjectileRight, mGunRightSpawn.transform.position, mGunRightSpawn.transform.rotation);
                currentmGunAmmunition -=1;
                TextMeshProUGUI test = GameObject.FindGameObjectWithTag("AmmunitionTag").GetComponent<TextMeshProUGUI>();
                test.SetText("" + currentmGunAmmunition);
            }
            else
            {
                currentmGunAmmunition = 0;
                TextMeshProUGUI test = GameObject.FindGameObjectWithTag("AmmunitionTag").GetComponent<TextMeshProUGUI>();
                test.SetText("" + currentmGunAmmunition);
                mGunReloading = true;
                
            }
        }
    }


    public void Item_Lazer()
    {
        //lazer is collected, activated weapon and active item slot
        generalActiveItemCollected = true;
        currentActiveItem = "Item_Lazer";
        activeItemCanvas.SetActive(true);
        currentLazerAmmunition = lazerAmmunition;
        GameObject.FindGameObjectWithTag("ActiveItemSlot").GetComponent<ActiveItemManager>().SetCurrentActiveItem(1);
        TextMeshProUGUI test = GameObject.FindGameObjectWithTag("AmmunitionTag").GetComponent<TextMeshProUGUI>();
        test.SetText("" + currentLazerAmmunition);
    }

    public void LazerShoot()
    {
      
        if (generalActiveItemCollected && !lazerReloading)
        {
            if (currentLazerAmmunition > 1)
            {
                Debug.Log("Creating lazer");
                Instantiate(laserProjectile, laserProjectile.transform.position, laserProjectile.transform.rotation);
                currentLazerAmmunition -= 1;
                TextMeshProUGUI test = GameObject.FindGameObjectWithTag("AmmunitionTag").GetComponent<TextMeshProUGUI>();
                test.SetText("" + currentLazerAmmunition);
            }
            else
            {
                currentLazerAmmunition = 0;
                TextMeshProUGUI test = GameObject.FindGameObjectWithTag("AmmunitionTag").GetComponent<TextMeshProUGUI>();
                test.SetText("" + currentLazerAmmunition);
                lazerReloading = true;
               
            }
        }
    }


}
