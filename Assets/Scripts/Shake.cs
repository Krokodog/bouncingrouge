﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour
{
    public Animator camAnimShake;

    public void CamShake()
    {
        camAnimShake.SetTrigger("shake");
    }
}
