﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestItemSpawner : MonoBehaviour {

    public GameObject testItemPositive;
    public GameObject testItemNegative;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.W))
        {
            Instantiate(testItemPositive, this.transform.position, this.transform.rotation);
            Debug.Log("W");
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Instantiate(testItemNegative, this.transform.position, this.transform.rotation);
        }

    }
}
